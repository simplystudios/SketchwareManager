package io.sketchware.java.wrapper.common

fun interface OnActionFinishedCallback {
    fun onFinish()
}